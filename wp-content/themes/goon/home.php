<?php
/**
 * Template Name: Blog
 */

get_header('service'); ?>

    <div class="blog">

        <div class="wrapper-inner">

            <a class="s-service-back-to-home" href="<?php echo get_home_url(); ?>">Powrót</a>

            <h1>Blog</h1>

            <div class="blog-list">

				<?php
				$paged    = ( get_query_var( 'paged' ) ) ? absint( get_query_var( 'paged' ) ) : 1;
				$args     = array(
					'posts_per_page' => 9,
					'post_type'      => 'post',
					'paged'          => $paged,
				);
				$wp_query = new WP_Query( $args );
				if ( $wp_query->have_posts() ) {
					while ( $wp_query->have_posts() ) {
						$wp_query->the_post(); ?>
                        <a class="blog-item" href="<?php the_permalink(); ?>">
                        <figure><?php the_post_thumbnail( 'full', array( 'alt' => 'some text' ) ); ?></figure>
                        <h3><?php the_title(); ?></h3>
                        <time><?php the_modified_date(); ?></time>
                        </a><?php
					}
				} else {
					echo 'No any posts';
				}
				?>

            </div>

			<?php
			$args = array(
				'show_all'           => false,
				'end_size'           => 1,
				'mid_size'           => 1,
				'prev_next'          => true,
				'prev_text'          => __( '&lt; Previous' ),
				'next_text'          => __( 'Next &gt;' ),
				'add_args'           => false,
				'add_fragment'       => '',
				'screen_reader_text' => __( ' ' ),
			);
			the_posts_pagination( $args );
			?>

        </div>

    </div>

<?php
get_footer('service');
?>