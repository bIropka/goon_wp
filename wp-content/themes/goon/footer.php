<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 */

?>

</main>

<footer>

    <div class="wrapper-inner">

        <ul class="social-links">
	        <?php
	        $social_link = get_field( 'social_link', 10 );
	        if ( $social_link ) {
		        foreach ( $social_link as $key => $value ) {
			        ?>
                    <li>
                        <a href="<?php echo $value['social_link_href']; ?>">
                            <img src="<?php echo $value['social_link_img']; ?>"
                                 alt="<?php echo $value['social_link_alt']; ?>">
                        </a>
                    </li>
			        <?php
		        }
	        }
	        ?>
        </ul>

        <div class="developer">Realizacja: <a href="https://www.mx-studio.pl/" target="_blank">MX Studio</a></div>

    </div>

</footer>

</div> <!-- end of wrapper -->

<script src="<?php echo get_template_directory_uri(); ?>/assets/public/js/jquery.min.js"></script>
<script src="<?php echo get_template_directory_uri(); ?>/assets/public/js/scripts.min.js"></script>

<?php wp_footer(); ?>
</body>

</html>