<?php
/**
 * Template Name: Page service simple
 */

get_header('service'); ?>

<div class="s-service-simple">

    <div class="wrapper-inner">

        <div class="s-service-header">

            <h1><?php the_field('page_service_title'); ?></h1>

            <a class="s-service-back-to-home" href="<?php echo get_home_url(); ?>#services">Powrót</a>

        </div>

        <div class="s-service-text">

		    <?php
		    $page_service_text = get_field( 'page_service_text' );
		    if ( $page_service_text ) {
			    foreach ( $page_service_text as $key => $value ) {
				    echo $value['text'];
			    }
		    }
		    ?>

        </div>

    </div>

</div>

<?php
get_footer('service');
?>
