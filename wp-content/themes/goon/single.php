<?php
/**
 * Template Name: Single
 */

get_header('service'); ?>

    <div class="blog">

        <div class="wrapper-inner">

            <a class="s-service-back-to-home" href="<?php echo get_home_url(); ?>/blog">Powrót</a>

            <article>

                <?php the_post(); ?>

                <h1><?php the_title(); ?></h1>

                <div class="article-image-main"><?php the_post_thumbnail( 'full', array( 'alt' => 'some text' ) ); ?></div>

	            <?php the_content(); ?>

            </article>

        </div>

    </div>

<?php
get_footer('service');
?>