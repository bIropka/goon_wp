<?php
/**
 * Template Name: Page service
 */

get_header('service'); ?>

<div class="s-service">

    <div class="wrapper-inner">

        <div class="s-service-header">

            <h1><?php the_field('page_service_title'); ?></h1>

            <a class="s-service-back-to-home" href="<?php echo get_home_url(); ?>#services">Powrót</a>

        </div>

        <div class="s-service-info">

            <div class="s-service-text">

	            <?php
	            $page_service_text = get_field( 'page_service_text' );
	            if ( $page_service_text ) {
		            foreach ( $page_service_text as $key => $value ) {
			            echo $value['text'];
		            }
	            }
	            ?>

            </div>

            <ul class="s-service-list">

	            <?php
	            $page_service_list = get_field( 'page_service_list' );
	            if ( $page_service_list ) {
		            foreach ( $page_service_list as $key => $value ) {
			            ?>
                        <li><?php echo $value['text']; ?></li>
			            <?php
		            }
	            }
	            ?>

            </ul>

        </div>

        <div class="s-service-image"><img src="<?php the_field('page_service_image'); ?>" alt="<?php the_field('page_service_image_alt'); ?>"></div>

    </div>

</div>

<?php
get_footer('service');
?>
