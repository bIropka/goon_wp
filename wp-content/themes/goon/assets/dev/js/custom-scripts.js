$(window).ready(function() {

    var wrapper = $('.wrapper');

    setTimeout(function () {
        wrapper.animate({opacity: 1}, 300);
    }, 300);

    $(window).scroll(function() {

        if($(window).scrollTop() > 20) {
            wrapper.addClass('shortcut');
        } else {
            if($(window).width() > 991) {
                wrapper.removeClass('shortcut');
            }
        }

    });

    $('.nav-header a[href^="#"]').click(function(){

        var target = $(this).attr('href');

        $('html, body').animate({scrollTop: $(target).offset().top - 75}, 800);

        return false;

    });

    $('.burger').click(function() {
        $(this).toggleClass('active');
        $('.nav-header').toggleClass('active');
    });

    if($(window).width() < 992) {
        wrapper.addClass('shortcut');
    } else {
        if($(window).scrollTop() < 21) {
            wrapper.removeClass('shortcut');
        }
    }

    $(window).resize(function() {
        if($(window).width() < 992) {
            wrapper.addClass('shortcut');
        } else {
            if($(window).scrollTop() < 21) {
                wrapper.removeClass('shortcut');
            }
        }
    });

    var services = $('#services');
    if(parseInt($(window).scrollTop()) === parseInt(services.offset().top + '')) {
        $('html, body').animate({scrollTop: services.offset().top - 60}, 0);
    }

    if($(window).scrollTop() > 20) {
        wrapper.addClass('shortcut');
    } else {
        if($(window).width() > 991) {
            wrapper.removeClass('shortcut');
        }
    }

    /* sliders */

    var portfolio = $('.portfolio-slider'),
        logos = $('.logos-slider'),
        logosSize = $('.logos-slider .slide').length;

    if (portfolio) {
        portfolio.slick({
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        autoplay: true
                    }
                }
            ]
        });
    }

    if (logos) {

        logos.slick({
            slidesToShow: 6,
            autoplay: true,
            autoplaySpeed: 0,
            speed: 2000,
            swipeToSlide: true,
            arrows: false,
            cssEase: 'linear',
            responsive: [
                {
                    breakpoint: 980,
                    settings: {
                        slidesToShow: 5
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4
                    }
                },
                {
                    breakpoint: 640,
                    settings: {
                        slidesToShow: 3
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 2
                    }
                },
                {
                    breakpoint: 360,
                    settings: {
                        slidesToShow: 1
                    }
                }
            ]
        });

        $('.logos .slider-arrow').hover(
            function() {
                logos.slick('slickPause');
            },
            function () {
                logos.slick('slickPlay');
                logos.slick('slickSetOption', 'speed', 2000);
            }
        );

        $('.logos .slider-prev').click(function() {
            var slidesToScroll = getSlidesToScroll();
            var currentSlide = logos.slick('slickCurrentSlide');
            logos.slick('slickSetOption', 'speed', 400);

            if(currentSlide - slidesToScroll < 0) {
                logos.slick('slickGoTo', currentSlide - slidesToScroll + logosSize);
            } else {
                logos.slick('slickGoTo', currentSlide - slidesToScroll);
            }

        });

        $('.logos .slider-next').click(function() {
            var slidesToScroll = getSlidesToScroll();
            var currentSlide = logos.slick('slickCurrentSlide');
            logos.slick('slickSetOption', 'speed', 400);

            if(currentSlide + slidesToScroll > logosSize) {
                logos.slick('slickGoTo', slidesToScroll + logosSize - currentSlide);
            } else {
                logos.slick('slickGoTo', currentSlide + slidesToScroll);
            }

        });

        function getSlidesToScroll() {
            var slidesToScroll = 6;
            if($(window).width() < 360) {
                slidesToScroll = 1;
            } else if($(window).width() < 480) {
                slidesToScroll = 2;
            } else if($(window).width() < 640) {
                slidesToScroll = 3;
            } else if($(window).width() < 768) {
                slidesToScroll = 4;
            } else if($(window).width() < 980) {
                slidesToScroll = 5;
            }
            return slidesToScroll;
        }

    }

    /* end of sliders */

});