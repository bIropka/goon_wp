var gulp = require('gulp'),
    pug = require('gulp-pug'),
    browsersync = require('browser-sync'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglifyjs'),
    rename = require('gulp-rename'),
    del = require('del'),
    sass = require('gulp-sass'),
    plumber = require("gulp-plumber"),
    notify = require("gulp-notify"),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css');

/////////////////////////////////
/////////// scss
/////////////////////////////////

gulp.task('styles', function () {
    return gulp.src('dev/scss/main.scss')
        .pipe(rename("custom-styles.scss"))
        .pipe(plumber())
        .pipe(sourcemaps.init())
        .pipe(sass())
        .on("error", notify.onError(function(error) {
            return "Something happened: " + error.message;
        }))
        .pipe(autoprefixer(['last 2 version']))
        .pipe(cleanCSS())
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('public/css'))
        .pipe(browsersync.reload({
            stream: true
        }));
});

gulp.task('styles-prod', function () {
    return gulp.src('dev/scss/main.scss')
        .pipe(rename("custom-styles.scss"))
        .pipe(plumber())
        .pipe(sass())
        .on("error", notify.onError(function(error) {
            return "Something happened: " + error.message;
        }))
        .pipe(autoprefixer(['last 2 version']))
        .pipe(cleanCSS())
        .pipe(gulp.dest('public/css'))
        .pipe(browsersync.reload({
            stream: true
        }));
});

/////////////////////////////////
/////////// markup
/////////////////////////////////

gulp.task('markup', function() {
    return gulp.src('dev/markup/pages/*.pug')
        .pipe(plumber())
        .pipe(pug({
            pretty: true
        }))
        .on("error", notify.onError(function(error) {
            return "Something happened: " + error.message;
        }))
        .pipe(gulp.dest('public'))
        .pipe(browsersync.reload({
            stream: true
        }));
});

/////////////////////////////////
/////////// browsersync
/////////////////////////////////

gulp.task('browsersync', function() {
    browsersync({
        server: {
            baseDir: 'public'
        }
    });
});

/////////////////////////////////
/////////// scripts
/////////////////////////////////

gulp.task('scripts', function() {
    return gulp.src([
        'node_modules/slick-carousel/slick/slick.min.js',
        'dev/js/custom-scripts.js'
    ])
        .pipe(concat('scripts.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('public/js'))
        .pipe(browsersync.reload({
            stream: true
        }));
});

gulp.task('jquery', function() {
    return gulp.src([
        'node_modules/jquery/dist/jquery.min.js'
    ])
        .pipe(gulp.dest('public/js'))
});

/////////////////////////////////
/////////// images
/////////////////////////////////

gulp.task('images', function() {
    return gulp.src(['dev/images/**/*.*'])
        .pipe(gulp.dest('public/images'))
        .pipe(browsersync.reload({
            stream: true
        }));
});

/////////////////////////////////
/////////// fonts
/////////////////////////////////

gulp.task('fonts', function() {
    return gulp.src('dev/fonts/**/*.*')
        .pipe(gulp.dest('public/fonts'))
        .pipe(browsersync.reload({
            stream: true
        }));
});

/////////////////////////////////
/////////// watch
/////////////////////////////////

gulp.task('watch', [ 'browsersync', 'build' ], function() {
    gulp.watch('dev/scss/**/*.scss', ['styles']);
    gulp.watch('dev/markup/**/*.pug', ['markup']);
    gulp.watch(['dev/js/**/*.js', 'dev/libs/**/*.js'], ['scripts']);
    gulp.watch('dev/images/**/*.*', ['images']);
    gulp.watch('dev/fonts/**/*.*', ['fonts']);
});

/////////////////////////////////
/////////// clean
/////////////////////////////////

gulp.task('clean', function() {
    return del.sync('public');
});

/////////////////////////////////
/////////// build
/////////////////////////////////

gulp.task('build', [ 'markup', 'styles-prod', 'jquery', 'scripts', 'images', 'fonts' ]);

/////////////////////////////////
/////////// default
/////////////////////////////////
gulp.task('default', ['watch']);
