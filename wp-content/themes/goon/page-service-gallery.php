<?php
/**
 * Template Name: Page service gallery
 */

get_header('service'); ?>

<div class="s-service">

    <div class="wrapper-inner">

        <div class="s-service-header">

            <h1><?php the_field('page_service_title'); ?></h1>

            <a class="s-service-back-to-home" href="<?php echo get_home_url(); ?>#services">Powrót</a>

        </div>

        <div class="s-service-gallery">

		    <?php
		    $page_service_gallery = get_field( 'page_service_gallery' );
		    if ( $page_service_gallery ) {
			    foreach ( $page_service_gallery as $key => $value ) {
				    ?>
                    <div class="s-service-gallery-item">

                        <h3><?php echo $value['title']; ?></h3>

                        <div class="s-service-gallery-image">
                            <img src="<?php echo $value['image']; ?>" alt="<?php echo $value['image_alt']; ?>">
                        </div>

                    </div>
				    <?php
			    }
		    }
		    ?>

        </div>

    </div>

</div>

<?php
get_footer('service');
?>
