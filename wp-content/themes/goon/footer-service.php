<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 */

?>

</main>

</div> <!-- end of wrapper -->

<script src="<?php echo get_template_directory_uri(); ?>/assets/public/js/jquery.min.js"></script>
<script>

    var wrapper = $('.wrapper');

    setTimeout(function () {
        wrapper.animate({opacity: 1}, 300);
    }, 300);

</script>

<?php wp_footer(); ?>
</body>

</html>