<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <main>
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js no-svg">
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title><?php echo wp_get_document_title(); ?></title>
	<?php wp_head(); ?>
    <link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/assets/public/css/custom-styles.css">
</head>

<body <?php body_class(); ?>>

<div id="page" class="wrapper site">

    <header>

        <div class="wrapper-inner">

            <div class="logo"><img src="<?php the_field('general_logo_image'); ?>" alt="<?php the_field('general_logo_alt'); ?>"></div>

	        <?php
	        wp_nav_menu( array(
		        'menu_class' => 'menu',
		        'container'=>'nav',
		        'container_class' => 'nav-header'
	        ) );
	        ?>

            <div class="burger">
                <div class="burger-top"></div>
                <div class="burger-center"></div>
                <div class="burger-bottom"></div>
            </div>

        </div>

    </header>

    <main>