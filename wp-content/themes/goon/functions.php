<?php

register_nav_menus( array(
	'top' => 'Main menu'
) );

function remove_admin_login_header() {
	remove_action( 'wp_head', '_admin_bar_bump_cb' );
}
add_action( 'get_header', 'remove_admin_login_header' );

add_theme_support( 'post-thumbnails', array( 'post' ) );

add_action( 'save_post', 'wptuts_save_thumbnail' );

function wptuts_save_thumbnail( $post_id ) {

	$post_thumbnail = get_post_meta( $post_id, $key = '_thumbnail_id', $single = true );

	if ( !wp_is_post_revision( $post_id ) ) {
		if ( empty( $post_thumbnail ) ) {
			update_post_meta( $post_id, $meta_key = '_thumbnail_id', $meta_value = '288' );
		}
	}

}